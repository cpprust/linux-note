A note for after installing Linux

distro: Endeavour

desktop: Plasma

## Config

- Mouse > Pointer acceleration > set to "None"

- Accessibility > Shake Cursor > uncheck "Enable"

- Virtual Keyboard > select "Fcitx5"

- Input Method > Add Input Method

Add whatever you want, mine is "Chewing"

- Input Method > Configure global options > Trigger Input Method

Change "Ctrl+Space" to "Super+Space"

