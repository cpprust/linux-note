sudo pacman -S git --noconfirm                              # distributed version control system
git config --global init.defaultBranch main                 # set default branch name to main
sudo pacman -S neovim --noconfirm                           # text editor
git clone https://github.com/LazyVim/starter ~/.config/nvim # lazyvim config
sudo pacman -S ttf-hack-nerd --noconfirm                    # nerd font for nvim
sudo pacman -S wl-clipboard --noconfirm                     # wayland clipboard tool for nvim
echo EDITOR=nvim | sudo tee -a /etc/environment             # set default editor to nvim
rustup default nightly                                      # use rust nightly features
sudo pacman -S clang --noconfirm                            # c++ language server
sudo pacman -S uv --noconfirm                               # python package manager
uv tool install basedpyright                                # python language server
uv tool update-shell                                        # ensure "~/.local/bin" in PATH
sudo pacman -S typescript-language-server --noconfirm       # typescript, javascript language server
sudo pacman -S vscode-css-languageserver --noconfirm        # css language server
sudo pacman -S vscode-html-languageserver --noconfirm       # html language server
sudo pacman -S vscode-json-languageserver --noconfirm       # json language server
sudo pacman -S bash-language-server --noconfirm             # bash language server
sudo pacman -S fcitx5-im --noconfirm                        # fcitx5 & config tool
sudo pacman -S fcitx5-chewing --noconfirm                   # chewing input method, replace this line with your input method
sudo pacman -S krita --noconfirm                            # draw
sudo pacman -S obs-studio --noconfirm                       # stream & record
sudo pacman -S telegram-desktop --noconfirm                 # chat
sudo pacman -S discord --noconfirm                          # chat
sudo pacman -S libreoffice-fresh --noconfirm                # office
sudo pacman -S wireshark-qt --noconfirm                     # network dianose tool
sudo usermod -a -G wireshark $USER                          # append user to wireshark group
sudo pacman -S flatpak --noconfirm                          # app installer
