#!/usr/bin/sh

# gtk,gl=on cause grab mouse freeze
# gtk,show-menubar=off cannot <ctrl> + <alt> + <g> grab mouse

qemu-system-x86_64 \
    -accel kvm -smp 8 \
    -m 16G \
    -drive file=disk.qcow2 \
    -cdrom ./EndeavourOS_Endeavour_neo-2024.09.22.iso \
    -display gtk \
    -device virtio-vga \
    -usb
